# -*- coding: utf-8 -*-
'''
サンプルアプリケーション自動テストスクリプト

【動作環境】
 ・Python
 ・Firefox

【必要ライブラリ】
 selenium (Python-binding)
 # pip install selenium

【参考】
http://selenium-python.readthedocs.io/index.html
'''

import unittest
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0
import time

class AutoTest(unittest.TestCase):

    url = 'http://192.168.56.101:3000/'

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Firefox()
        cls.driver.get(cls.url)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    '''
    テスト内容： 個人データを3件登録する
    期待値：一覧表に3件追加されること
    '''
    def test_Insert_3(self):

		# ユーザ管理をクリック
        self.__clickUser()

        # 1件目の登録
        self.__insertData(("aaa", "bbb", "ccc"))
        self.__checkElement('//form[@id="deleteForm"]/table/tbody/tr[1]/td[1]',
                            "aaa")

        # 2件目の登録
        self.__insertData(("ddd", "eee", "fff"))
        self.__checkElement('//form[@id="deleteForm"]/table/tbody/tr[2]/td[1]',
                            "ddd")

        # 3件目の登録
        self.__insertData(("ggg", "hhh", "iii"))
        self.__checkElement('//form[@id="deleteForm"]/table/tbody/tr[3]/td[1]',
                            "ggg")

    '''
    テスト内容： 個人データを1件削除する
    期待値：一覧表から削除されること
    '''
    def test_delete1(self):
        # 3件目の削除
        self.__deleteElement('//form[@id="deleteForm"]/table/tbody/tr[last()]/th/input')
        # expected_conditions.staleness_ofが使える？
        #self.__checkElement('//form[@id="deleteForm"]/table/tbody/tr[last()]/td[1]',
        #                    "ddd")

    '''
    テスト内容： 個人データを2件削除する
    期待値：一覧表に2件削除されること
    '''
    def test_delete2(self):
        pathlist = [
            '//form[@id="deleteForm"]/table/tbody/tr[1]/th/input',
            '//form[@id="deleteForm"]/table/tbody/tr[2]/th/input'
        ]

        self.__deleteElements(pathlist)
        # self.__checkElement('//form[@id="deleteForm"]/table/tbody/tr[last()]/td[1]',
        #                    "Canon Taro")
        self.__clickBack()

    def __clickUser(self):
        button = self.driver.find_element_by_xpath("/html/body/div[2]/div[1]/button")
        button.click()

    def __clickTask(self):
        button = self.driver.find_element_by_xpath("/html/body/div[2]/div[2]/button")
        button.click()

    def __clickBack(self):
        #link = self.driver.find_element_by_xpath("/html/body/a")
        link = self.driver.find_element_by_link_text("Back")
        link.click()
        time.sleep(.300)

    def __insertData(self, data):
        # フォーム要素の取得
        form = self.driver.find_element_by_id("inputForm")
        name = self.driver.find_element_by_id("name")
        company = self.driver.find_element_by_id("company")
        mail = self.driver.find_element_by_id("mail")

        # データ入力
        name.send_keys(data[0])
        company.send_keys(data[1])
        mail.send_keys(data[2])
        form.submit()
        time.sleep(.500)

    def __checkElement(self, path, expected):
        # 検証
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, path))
            )

        except:
            print "exception"
            import traceback
            traceback.print_exc()

        self.assertEqual(element.text, expected)

    def __deleteElement(self, path):
        # フォーム要素の取得
        form = self.driver.find_element_by_id("deleteForm")

        element = self.driver.find_element_by_xpath(path)
        element.click()

        form.submit()
        time.sleep(.500)

    def __deleteElements(self, pathlist):
        # フォーム要素の取得
        form = self.driver.find_element_by_id("deleteForm")

        for path in pathlist:
            element = self.driver.find_element_by_xpath(path)
            element.click()

        form.submit()
        time.sleep(.500)

if __name__ == '__main__':
    unittest.main()
