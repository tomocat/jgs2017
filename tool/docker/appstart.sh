#!/bin/bash

sudo docker stop myapp
sudo docker rm myapp

sudo docker run \
		--name myapp \
		-p 3000:3000 \
		--link my-mongo:mongodb \
		-d myregistry:5000/myapp
