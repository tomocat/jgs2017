#!/bin/bash

mongodb-linux-x86_64-3.4.1/bin/mongoimport --db test --collection restaurants --drop --file ./primer-dataset.json
mongodb-linux-x86_64-3.4.1/bin/mongoimport --db test --collection persons --drop --file ./sample.json
