var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
var assert = require('assert');

var URL_MAIN='http://' + process.env.DOCKER_HOST_ADDRESS + ':3000';
var URL_MONGO='mongodb://' + process.env.DOCKER_HOST_ADDRESS + ':27017/test';

/* GET home page. */
router.get('/', function(req, res, next) {
    // DBから一覧を取得する
	var persons;
	var tasks;
	dbWrapper('persons', function(coll) {
		persons  = coll.find();
		persons.toArray(function (err, personsArray) {
    		dbWrapper('tasks', function(coll) {
				tasks = coll.find();
				tasks.toArray(function (err, tasksArray) {
            		res.render('task', { persons: personsArray,
                                          tasks: tasksArray,
					                      main: URL_MAIN });
				});
			});
		});
	});
});

router.post('/add', function(req, res, next) {

    var data = req.body;

    dbWrapper('tasks', function(coll) {
        coll.insertOne(data, function(err, r){
            assert.equal(null, err);
            assert.equal(1, r.insertedCount);
    		res.redirect('/task');
        });
    });
});

router.post('/del', function(req, res, next) {

    var data = req.body;
    var ids = data._id; // 複数の場合はarray
    var count = 0;

	if(ids instanceof Array){
    	for(i in ids) {
        	var tmp = {};
        	tmp._id = ObjectID(ids[i]);
        	dbWrapper('tasks', function(coll, id) {
            	console.log("delete1: " + id._id);
            	coll.deleteOne(id, function(err, r) {
                	assert.equal(null, err);
                	count++;
    				if(count == ids.length-1)
        				res.redirect('/task');
            	});
        	}, tmp);
    	}
	} else {
       	var tmp = {};
		tmp._id = ObjectID(ids);
        dbWrapper('tasks', function(coll, id) {
            // console.log(JSON.stringify(id));
            coll.deleteOne(id, function(err, r) {
                assert.equal(null, err);
        		res.redirect('/task');
            });
        }, tmp);
	}

});

function getCollection(db, tableName) {
    return db.collection(tableName);
}

function dbWrapper(tableName, callback, param) {
    MongoClient.connect(URL_MONGO, function (err, db) {
        assert.equal(null, err);
        var coll = getCollection(db, tableName);
        callback(coll, param);
        db.close();
    });
}

module.exports = router;
