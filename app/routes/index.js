var express = require('express');
var router = express.Router();

// var URL_USER='http://' + process.env.DOCKER_HOST_ADDRESS + '/user';
// var URL_TASK='http://' + process.env.DOCKER_HOST_ADDRESS + '/task';
var URL_USER='/user';
var URL_TASK='/task';

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('index', { title: 'JGS2017 Sample',
                          user: URL_USER,
                          task: URL_TASK });
});

module.exports = router;
