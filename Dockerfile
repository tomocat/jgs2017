FROM node:6.9-alpine

ENV APP_DIR /srv/app

COPY app $APP_DIR
WORKDIR $APP_DIR

EXPOSE 3000
ENTRYPOINT ["./run.sh"]
